package com.example.ds;

/**
 * Created by rahul on 17/11/16.
 */
/*Q1. Given a string you need to print all possible strings that can be made by placing spaces (zero or one) in between them. For example : ABC -> A BC, AB C, ABC, A B C*/
public class StringWithSpace {

    public static void main(String[] str) {
        StringWithSpace s = new StringWithSpace();
        s.printSpace("ABCDEFGH");
    }


  /*  void printSpace(String str) {
        subString(str.toCharArray(), str.length() - 1, 1);
    }
*/

    void subString(char[] chars, int len, int index) {
        if (len > 0 && len < chars.length) {
            int i = 0;
            int count = 0;

            for (char ch : chars) {
                System.out.print(ch);
                i++;
                count++;
                if (i == len || count == index) {
                    System.out.print(" ");
                    i = 0;
                }
            }
            System.out.print("\n");
            if (index < len) {
                subString(chars, len, index + 1);
            } else {
                subString(chars, len - 1, 1);
            }
        }
    }

    void printSpace(String str) {
        // Your code here
        if (str != null && str.length() > 0) {
            printSpace(str, 1, String.valueOf(str.charAt(0)));
        }

    }

    void printSpace(String str, int i, String spaceString) {
        if (i >= str.length()) {
            System.out.print(spaceString.concat("$"));
            return;
        }

        printSpace(str, i + 1, spaceString + str.charAt(i));
        printSpace(str, i + 1, spaceString.concat(" ") + str.charAt(i));

    }
}
