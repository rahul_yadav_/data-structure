package com.example.ds.sorting;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by rahul on 17/06/16.
 */
public class Main {
    final Class sorting[] = new Class[]{BubbleSort.class};
    private Scanner sc;

    private Main() {
        sc = new Scanner(System.in);
        System.out.println("Press 0 for exit");
    }

    public Class getSortingClass() {
        int pos = 1;
        for (Class aClass : sorting) {
            System.out.println(pos + " : " + aClass.getSimpleName());
        }

        System.out.println("\nChoose sorting option : ");
        int op = getInput();

        if (op <= 0 || op > sorting.length) {
            return null;
        }

        return sorting[pos - 1];
    }


    public static void main(String[] arg) {
        Main main = new Main();
        Class aClass = main.getSortingClass();
        if (aClass == null) {
            System.out.println("Wrong Option choose ");
            return;
        }
        System.out.println("Sorting by " + aClass.getSimpleName());

        int[] array = {4, 3, 6, 5, 7, -7, -2, 1, 9, 0, 8, -10};
        System.out.println("Before sorting " + Arrays.toString(array));
        BubbleSort.sortAs(array);
        System.out.println("After sorting ASC" + Arrays.toString(array));
        BubbleSort.sortDes(array);
        System.out.println("After sorting DESC" + Arrays.toString(array));
    }

    public int getInput() {
        int op = 0;

        try {
            op = Integer.valueOf(sc.next());
        } catch (Exception e) {
            return -1;
        }
        if (op == 0) {
            System.exit(0);
        }

        return op;
    }

}
