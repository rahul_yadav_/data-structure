package com.example.ds.sorting;

import java.util.Arrays;

/**
 * Created by rahul on 17/06/16.
 */
public class BubbleSort {
    public static void sortAs(int... array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    array[j] = array[j] + array[i];
                    array[i] = array[j] - array[i];
                    array[j] = array[j] - array[i];
                }
            }
        }
    }

    public static void sortDes(int... array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] > array[i]) {
                    array[j] = array[j] + array[i];
                    array[i] = array[j] - array[i];
                    array[j] = array[j] - array[i];
                }
            }
        }
    }

    public static void main(String[] arg) {

        System.out.println("Sorting by BubbleSort");

        int[] array = {4, 3, 6, 5, 7, -7, -2, 1, 9, 0, 8, -10};
        System.out.println("Before sorting " + Arrays.toString(array));
        sortAs(array);
        System.out.println("After sorting ASC" + Arrays.toString(array));
        sortDes(array);
        System.out.println("After sorting DESC" + Arrays.toString(array));
    }
}
