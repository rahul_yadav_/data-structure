package com.example.ds.sorting;

import java.util.Arrays;

/**
 * Created by rahul on 17/06/16.
 */
public class SelectionSort {

    public static void sortAs(int... array) {
        for (int i = 0; i < array.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[index]) {
                    index = j;
                }
            }
            int smallerNumber = array[index];
            array[index] = array[i];
            array[i] = smallerNumber;
        }
    }

    public static void sortDes(int... array) {
        for (int i = 0; i < array.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] > array[index]) {
                    index = j;
                }
            }
            int bigNumber = array[index];
            array[index] = array[i];
            array[i] = bigNumber;
        }
    }

    public static void main(String[] arg) {

        System.out.println("Sorting by Selection Sort");

        int[] array = {-20, 4, 3, 6, 5, 7, -7, -2, 3, 1, 9, 0, 8, -10};
        System.out.println("Before sorting " + Arrays.toString(array));
        sortAs(array);
        System.out.println("After sorting ASC" + Arrays.toString(array));
        sortDes(array);
        System.out.println("After sorting DESC" + Arrays.toString(array));
    }
}
