package com.example.ds.sorting;

import java.util.Arrays;

/**
 * Created by rahul on 17/06/16.
 */
public class InsertionSort {

    public static void sortAs(int... array) {
        for (int k = array.length; k >= 0; k--) {
            for (int i = 1; i < k; i++) {
                int j = i - 1;
                if (array[i] < array[j]) {
                    int s = array[i];
                    array[i] = array[j];
                    array[j] = s;
                }
            }
        }
    }

    public static void sortDes(int... array) {
        for (int k = array.length; k >= 0; k--) {
            for (int i = 1; i < k; i++) {
                int j = i - 1;
                if (array[i] > array[j]) {
                    int s = array[i];
                    array[i] = array[j];
                    array[j] = s;
                }
            }
        }
    }

    public static void main(String[] arg) {

        System.out.println("Sorting by InsertionSort");

        int[] array = {4, 3, 6, 5, 7, -7, -2, 1, 9, 0, 8, -10};
        System.out.println("Before sorting " + Arrays.toString(array));
        sortAs(array);
        System.out.println("After sorting ASC" + Arrays.toString(array));
        sortDes(array);
        System.out.println("After sorting DESC" + Arrays.toString(array));
    }
}
